﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MyPage.Models
{
    public class ModelBase
    {
        [DefaultValue("true")]
        public bool IsActive { get; set; }
        [DefaultValue(1)]
        public long CreatedBy { get; set; }
        [DefaultValue(1)]
        public long UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int Status { get; set; }
    }
}
