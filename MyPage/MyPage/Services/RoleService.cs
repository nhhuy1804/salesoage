﻿using MyPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyPage.Services
{
    public interface IRoleService
    {
        IEnumerable<ApplicationRole> GetRole(int PageSize, int Page);
        bool CreateRole(AppRoleViewModel model);
        bool DeleteRole(List<string> data);
    }
    public class RoleService : IRoleService
    {
        private readonly DBContext _dbContext;
        public RoleService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<ApplicationRole> GetRole(int PageSize, int Page)
        {
            return _dbContext.ApplicationRoles.Where(x => x.IsActive).OrderByDescending(x => x.CreatedDate).Skip((Page - 1) * PageSize).Take(PageSize);
        }

        public bool CreateRole(AppRoleViewModel model)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    _dbContext.ApplicationRoles.Add(new ApplicationRole
                    {
                        CreatedBy = "1",
                        CreatedDate = DateTime.Now,
                        IsActive = true,
                        Name = model.name,
                        NormalizedName = model.normalizedName,
                        Status = 1,
                        UpdatedBy = "1",
                        UpdatedDate = DateTime.Now
                    });
                    _dbContext.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }

        public bool DeleteRole(List<string> data)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    foreach (var item in data)
                    {
                        var model = _dbContext.ApplicationRoles.Where(x => x.Id == item).FirstOrDefault();
                        _dbContext.ApplicationRoles.Attach(model);
                        model.IsActive = false;
                    }
                    _dbContext.SaveChanges();
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }
    }
}
